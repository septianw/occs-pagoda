#!/bin/bash
pwd
#cp -Rf core/config-rm etc/config
#cp -Rf core/apps-rm etc/apps
#rm -Rf etc/config/config-rm
#rm -Rf etc/apps/apps-rm
#cd etc
if [ ! -f apps ]
then
  cp -Rf core/apps-rm apps
#  ln -s ../apps
fi

if [ ! -f config ]
then
  mkdir config
  cd config
else
  cd config
fi

if [ ! -f config.php ]
then
  wget https://raw.github.com/septianw/oc-config/master/config.php
fi
#rm etc/config.php
#cp config/config.php etc/config/config.php
